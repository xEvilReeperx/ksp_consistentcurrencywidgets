﻿/******************************************************************************
               AlignedCurrencyIndicator for Kerbal Space Program                    
 ******************************************************************************
Copyright (c) 2014 Allen Mrazek

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
******************************************************************************/
using System.Collections.Generic;
using ReeperCommon;
using UnityEngine;
using System.Globalization;

namespace ConsistentCurrencyWidgets
{
    /// <summary>
    /// Remove the tumbler-style funds widget and replace it with a stock-style calculator widget. This
    /// also modifies the stock cost widget, adding locale formatting and removing strategy text
    /// additions of override bool is set
    /// </summary>
    class MatchCostWidget : MonoBehaviour
    {
        public bool overrideStockCostLogic = false;

        /*
        [LOG 20:27:27.438] AlignedMoneyIndicator, CurrencyWidgets_Editor has components: <--- we're attached to this GO
        [LOG 20:27:27.439] AlignedMoneyIndicator, ...c: UnityEngine.Transform
        [LOG 20:27:27.440] AlignedMoneyIndicator, ...c: NestedPrefabSpawner
        [LOG 20:27:27.441] AlignedMoneyIndicator, --->FundsWidget has components:
        [LOG 20:27:27.442] AlignedMoneyIndicator, ......c: UnityEngine.Transform
        [LOG 20:27:27.443] AlignedMoneyIndicator, ......c: FundsWidget
                                                            [ snip children ]
        [LOG 20:27:27.489] AlignedMoneyIndicator, --->CostWidget has components:
        [LOG 20:27:27.489] AlignedMoneyIndicator, ......c: UnityEngine.Transform
        [LOG 20:27:27.490] AlignedMoneyIndicator, ......c: CostWidget
                                                            [ snip children ]
         * */

        // note to self: this will be attached to the CurrencyWidgets_Editor GO
        private void Start()
        {
            CostWidget costWidget = transform.GetComponentInChildren<CostWidget>();
            FundsWidget fundsWidget = transform.GetComponentInChildren<FundsWidget>();

            if (costWidget == null) Log.Error("MatchCostWidget: Failed to find CostWidget");
            if (fundsWidget == null) Log.Error("MatchCostWidget: failed to find FundsWidget");

#if DEBUG
            costWidget.gameObject.PrintComponents();
#endif

            // clone the stock cost widget
            Log.Debug("Cloning stock cost widget");
            var costClone = (GameObject)Instantiate(costWidget.gameObject);

            // position it over the funds widget
            costClone.transform.position = fundsWidget.transform.position;
            costClone.transform.parent = transform;

            // hide the tumbler style stock funds widget
#if DEBUG
            // move it instead, for comparison against clone
            fundsWidget.gameObject.transform.Translate(new Vector3(300f, 0f, 0f), Space.Self);

            // temp:
            costClone.transform.Translate(new Vector3(300f, 200f, 0f), Space.Self);

            costClone.transform.Find("Frame").gameObject.SetActive(false);

#else
            Log.Debug("Hiding tumbler widget");
            fundsWidget.gameObject.SetActive(false);
#endif
            // the GameObject we cloned comes with the red currency icon; we want to use
            // the green one from fundsWidget
            Log.Debug("Editing cloned cost widget icon to match green funds");
            costClone.transform.Find("costIcon").renderer.material.mainTexture = fundsWidget.transform.Find("fundsGreen").renderer.sharedMaterial.mainTexture;


            // The clone we made will be listening for cost events instead of funds like it should be,
            // so that logic needs replacement

            costClone.AddComponent<FundsLogicOverride>();

            // The stock cost widget has some weird scaling when strategies are in effect
            // along with listing what I consider to be an unnecessary and ugly addition of
            // showing the exact percent increase in parenthensis, so we give the player
            // the option of overwriting it
            if (this.overrideStockCostLogic)
                costWidget.gameObject.AddComponent<CostLogicOverride>();
            else costWidget.gameObject.AddComponent<ModifiableText>(); // will simply fix text color instead
        }
    }


    /// <summary>
    /// Both logic overrides share some functionality
    /// </summary>
    class ModifiableText : MonoBehaviour
    {
        protected NumberFormatInfo formatter;
        protected TextMesh text;


        private void Awake()
        {
            formatter = (NumberFormatInfo)NumberFormatInfo.CurrentInfo.Clone();
            formatter.CurrencySymbol = string.Empty;
            formatter.CurrencyDecimalDigits = 0;

            text = GetComponentInChildren<TextMesh>();
            if (text == null) Log.Error("Failed to find TextMesh");

            text.renderer.material.color = Color.white; // it's set to something off-white which affects TextMesh color later


            var cw = GetComponent<CostWidget>();
            if (cw == null) Log.Error("FormatLogicOverride: Failed to find CostWidget");
            text.color = cw.affordableColor;
        }
    }



    /// <summary>
    /// Replaces stock unformatted cost widget text with locale-formatted version
    /// </summary>
    class CostLogicOverride : ModifiableText
    {
        Color affordableColor = Color.white;
        Color unaffordableColor = Color.red;
        Color irrelevantColor = Color.white;

        private System.Collections.IEnumerator Start()
        {
            Log.Debug("CostLogicOverride.Start");

            var cw = GetComponent<CostWidget>();
            text = GetComponentInChildren<TextMesh>();

            if (cw == null) Log.Error("CostLogicOverride: failed to find CostWidget");
            if (text == null) Log.Error("CostLogicOverride: failed to find TextMesh");

            affordableColor = cw.affordableColor;
            unaffordableColor = cw.unaffordableColor;
            irrelevantColor = cw.irrelevantColor;
            
            Component.DestroyImmediate(GetComponent<CostWidget>());
            GameEvents.onEditorShipModified.Add(OnShipModified);
            GameEvents.onGUILaunchScreenVesselSelected.Add(OnShipSelected);

            SetCost(0);

            yield return 0; // wait till next frame in case editor loads a ship from cache

            OnShipModified(EditorLogic.fetch.ship);
        }


        private void OnDestroy() { GameEvents.onEditorShipModified.Remove(OnShipModified); GameEvents.onGUILaunchScreenVesselSelected.Remove(OnShipSelected); }


        private void OnShipModified(ShipConstruct ship)
        {
            Log.Debug("CostLogicOverride.OnShipModified");
            float dryCost = 0f;
            float fuelCost = 0f;
            float total = EditorLogic.fetch.ship.GetShipCosts(out dryCost, out fuelCost);
            var cm = CurrencyModifierQuery.RunQuery(TransactionReasons.VesselRollout, total, 0f, 0f);
            total += cm.GetEffectDelta(Currency.Funds);

            SetCost(total);
        }


        private void OnShipSelected(ShipTemplate template)
        {
            Log.Debug("CostLogicOverride.OnShipSelected");
            SetCost(template.totalCost);
        }


        private void SetCost(double total)
        {
            text.text = total.ToString("c", formatter);

            if (total > Funding.Instance.Funds)
            {
                text.color = unaffordableColor;
            }
            else text.color = affordableColor;
        }
    }



    /// <summary>
    /// Contains logic to keep cloned cost widget (which is to serve as a funds widget instead)
    /// up to date with locale-formatted currency text
    /// </summary>
    class FundsLogicOverride : ModifiableText
    {
        private System.Collections.IEnumerator Start()
        {
            Component.DestroyImmediate(GetComponent<CostWidget>());

            GameEvents.OnFundsChanged.Add(OnFundsChanged);

            yield return 0;

            OnFundsChanged(Funding.Instance.Funds, TransactionReasons.None);
        }

        private void OnDestroy() { GameEvents.OnFundsChanged.Remove(OnFundsChanged); }

        private void OnFundsChanged(double newValue, TransactionReasons reason)
        {
            Log.Debug("FundsLogicOverride: OnFundsChanged to {0}", newValue);

            text.text = newValue.ToString("c", formatter);
        }
    }
}
