﻿/******************************************************************************
               AlignedCurrencyIndicator for Kerbal Space Program                    
 ******************************************************************************
Copyright (c) 2014 Allen Mrazek

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
******************************************************************************/
using System.Collections.Generic;
using System.Linq;
using ReeperCommon;
using UnityEngine;

namespace ConsistentCurrencyWidgets
{
    /// <summary>
    /// Convert the calculator-style cost widget into the tumbler style of the stock funds widget
    /// </summary>
    class MatchFundsWidget : MonoBehaviour
    {
        GameObject fundsClone; // clone of the stock tumbler funds widget

        /*
LOG 13:46:46.916] ConsistentCurrencyWidgets, --->FundsWidget has components:
[LOG 13:46:46.916] ConsistentCurrencyWidgets, ......c: UnityEngine.Transform
[LOG 13:46:46.917] ConsistentCurrencyWidgets, ......c: FundsWidget
[LOG 13:46:46.918] ConsistentCurrencyWidgets, ------>fundsGreen has components:
[LOG 13:46:46.918] ConsistentCurrencyWidgets, .........c: UnityEngine.Transform
[LOG 13:46:46.919] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshFilter
[LOG 13:46:46.919] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshRenderer
[LOG 13:46:46.920] ConsistentCurrencyWidgets, ------>fundsRef has components:
[LOG 13:46:46.921] ConsistentCurrencyWidgets, .........c: UnityEngine.Transform
[LOG 13:46:46.921] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshFilter
[LOG 13:46:46.922] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshRenderer
[LOG 13:46:46.922] ConsistentCurrencyWidgets, ------>Bg has components:
[LOG 13:46:46.923] ConsistentCurrencyWidgets, .........c: UnityEngine.Transform
[LOG 13:46:46.924] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshFilter
[LOG 13:46:46.925] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshRenderer
[LOG 13:46:46.925] ConsistentCurrencyWidgets, ------>Frame has components:
[LOG 13:46:46.926] ConsistentCurrencyWidgets, .........c: UnityEngine.Transform
[LOG 13:46:46.926] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshFilter
[LOG 13:46:46.927] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshRenderer
[LOG 13:46:46.928] ConsistentCurrencyWidgets, ------>tumblerMaskBottom has components:
[LOG 13:46:46.928] ConsistentCurrencyWidgets, .........c: UnityEngine.Transform
[LOG 13:46:46.929] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshFilter
[LOG 13:46:46.930] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshRenderer
[LOG 13:46:46.930] ConsistentCurrencyWidgets, ------>tumblerMaskTop has components:
[LOG 13:46:46.931] ConsistentCurrencyWidgets, .........c: UnityEngine.Transform
[LOG 13:46:46.931] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshFilter
[LOG 13:46:46.932] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshRenderer
[LOG 13:46:46.933] ConsistentCurrencyWidgets, ------>tumblers has components:
[LOG 13:46:46.933] ConsistentCurrencyWidgets, .........c: UnityEngine.Transform
[LOG 13:46:46.934] ConsistentCurrencyWidgets, .........c: Tumblers
[LOG 13:46:46.935] ConsistentCurrencyWidgets, --------->tumbler0 has components:
[LOG 13:46:46.935] ConsistentCurrencyWidgets, ............c: UnityEngine.Transform
[LOG 13:46:46.936] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshFilter
[LOG 13:46:46.936] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshRenderer
[LOG 13:46:46.937] ConsistentCurrencyWidgets, ............c: Tumbler
[LOG 13:46:46.938] ConsistentCurrencyWidgets, --------->tumbler1 has components:
[LOG 13:46:46.938] ConsistentCurrencyWidgets, ............c: UnityEngine.Transform
[LOG 13:46:46.939] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshFilter
[LOG 13:46:46.940] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshRenderer
[LOG 13:46:46.940] ConsistentCurrencyWidgets, ............c: Tumbler
[LOG 13:46:46.941] ConsistentCurrencyWidgets, --------->tumbler2 has components:
[LOG 13:46:46.942] ConsistentCurrencyWidgets, ............c: UnityEngine.Transform
[LOG 13:46:46.943] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshFilter
[LOG 13:46:46.944] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshRenderer
[LOG 13:46:46.944] ConsistentCurrencyWidgets, ............c: Tumbler
[LOG 13:46:46.945] ConsistentCurrencyWidgets, --------->tumbler3 has components:
[LOG 13:46:46.945] ConsistentCurrencyWidgets, ............c: UnityEngine.Transform
[LOG 13:46:46.946] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshFilter
[LOG 13:46:46.947] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshRenderer
[LOG 13:46:46.947] ConsistentCurrencyWidgets, ............c: Tumbler
[LOG 13:46:46.948] ConsistentCurrencyWidgets, --------->tumbler4 has components:
[LOG 13:46:46.949] ConsistentCurrencyWidgets, ............c: UnityEngine.Transform
[LOG 13:46:46.949] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshFilter
[LOG 13:46:46.950] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshRenderer
[LOG 13:46:46.950] ConsistentCurrencyWidgets, ............c: Tumbler
[LOG 13:46:46.951] ConsistentCurrencyWidgets, --------->tumbler5 has components:
[LOG 13:46:46.952] ConsistentCurrencyWidgets, ............c: UnityEngine.Transform
[LOG 13:46:46.952] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshFilter
[LOG 13:46:46.953] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshRenderer
[LOG 13:46:46.954] ConsistentCurrencyWidgets, ............c: Tumbler
[LOG 13:46:46.955] ConsistentCurrencyWidgets, --------->tumbler6 has components:
[LOG 13:46:46.956] ConsistentCurrencyWidgets, ............c: UnityEngine.Transform
[LOG 13:46:46.956] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshFilter
[LOG 13:46:46.957] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshRenderer
[LOG 13:46:46.958] ConsistentCurrencyWidgets, ............c: Tumbler
[LOG 13:46:46.958] ConsistentCurrencyWidgets, --------->tumbler7 has components:
[LOG 13:46:46.959] ConsistentCurrencyWidgets, ............c: UnityEngine.Transform
[LOG 13:46:46.960] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshFilter
[LOG 13:46:46.960] ConsistentCurrencyWidgets, ............c: UnityEngine.MeshRenderer
[LOG 13:46:46.961] ConsistentCurrencyWidgets, ............c: Tumbler
[LOG 13:46:46.962] ConsistentCurrencyWidgets, ------>comma_separator has components:
[LOG 13:46:46.963] ConsistentCurrencyWidgets, .........c: UnityEngine.Transform
[LOG 13:46:46.964] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshFilter
[LOG 13:46:46.964] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshRenderer
[LOG 13:46:46.965] ConsistentCurrencyWidgets, ------>comma_separator has components:
[LOG 13:46:46.965] ConsistentCurrencyWidgets, .........c: UnityEngine.Transform
[LOG 13:46:46.966] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshFilter
[LOG 13:46:46.967] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshRenderer
[LOG 13:46:46.967] ConsistentCurrencyWidgets, --->CostWidget has components:
[LOG 13:46:46.968] ConsistentCurrencyWidgets, ......c: UnityEngine.Transform
[LOG 13:46:46.968] ConsistentCurrencyWidgets, ......c: CostWidget
[LOG 13:46:46.969] ConsistentCurrencyWidgets, ------>Frame has components:
[LOG 13:46:46.970] ConsistentCurrencyWidgets, .........c: UnityEngine.Transform
[LOG 13:46:46.970] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshFilter
[LOG 13:46:46.971] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshRenderer
[LOG 13:46:46.972] ConsistentCurrencyWidgets, ------>valueText has components:
[LOG 13:46:46.972] ConsistentCurrencyWidgets, .........c: UnityEngine.Transform
[LOG 13:46:46.973] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshRenderer
[LOG 13:46:46.973] ConsistentCurrencyWidgets, .........c: UnityEngine.TextMesh
[LOG 13:46:46.974] ConsistentCurrencyWidgets, ------>costIcon has components:
[LOG 13:46:46.975] ConsistentCurrencyWidgets, .........c: UnityEngine.Transform
[LOG 13:46:46.975] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshFilter
[LOG 13:46:46.976] ConsistentCurrencyWidgets, .........c: UnityEngine.MeshRenderer
         * */

        // note to self: this will be attached to the CurrencyWidgets_Editor GO
        System.Collections.IEnumerator Start()
        {

            var fundsWidget = gameObject.GetComponentInChildren<FundsWidget>();
            var costWidget = gameObject.GetComponentInChildren<CostWidget>();

            if (fundsWidget == null) Log.Error("MatchFundsWidget: failed to find stock funds tumbler widget");
            if (costWidget == null) Log.Error("MatchFundsWidget: failed to find stock cost calculator widget");

            if (fundsWidget == null || costWidget == null) yield break;


            // clone the funds widget...
            Log.Debug("Cloning funds widget");
            fundsClone = (GameObject)Instantiate(fundsWidget.gameObject);

            // move the new clone to the cost widget's position
            Log.Debug("Moving clone to cost widget position");
            fundsClone.transform.parent = fundsWidget.transform.parent;
            fundsClone.transform.position = costWidget.transform.position;
            fundsClone.transform.position = new Vector3(
                costWidget.transform.position.x, costWidget.transform.position.y, fundsWidget.transform.position.z);

            //fundsClone.layer = fundsWidget.gameObject.layer;

            //foreach (Transform t in fundsClone.transform)
            //    Log.Warning("Cloned layer: " + t.name + " is " + t.gameObject.layer);

            //foreach (Transform t in fundsWidget.transform)
            //    Log.Warning("Real layer: " + t.name + " is " + t.gameObject.layer);




            // The funds we just cloned has a component that automatically registers itself to receive
            // the wrong type of events (in this case, total funds) which could overwrite our hard work.
            // We don't want that...
            Log.Debug("Destroying cloned widget's FundsWidget component");
            Object.DestroyImmediate(fundsClone.GetComponent<FundsWidget>());

            // hide the stock cost widget
#if !DEBUG
            costWidget.gameObject.SetActive(false);
#else
            // move the stock cost widget 
            costWidget.gameObject.transform.Translate(new Vector3(200f, 0f, 0f), Space.Self);

            fundsWidget.gameObject.GetComponentsInChildren<Renderer>(true)
                .ToList()
                .ForEach(r => Log.Normal("original: {0} on layer {1}", r.name, r.gameObject.layer));

            fundsClone.gameObject.GetComponentsInChildren<Renderer>(true)
                .ToList()
                .ForEach(r => Log.Normal("clone: {0} on layer {1}", r.name, r.gameObject.layer));
#endif

            // we do want to borrow its red currency icon though
            fundsClone.transform.Find("fundsGreen").renderer.material.mainTexture = costWidget.transform.Find("costIcon").renderer.sharedMaterial.mainTexture;

            // it has a mask on top that will "delete" part of the background for the ui panel, so that's
            // not something we'll be needing
            //fundsClone.transform.Find("tumblerMaskTop").gameObject.SetActive(false);
            //fundsClone.transform.Find("tumblerMaskTop")
            //    .renderer.material.mainTexture.as2D()
            //    .CreateReadable()
            //    .SaveToDisk("tumblerMaskTop.png");

            Camera.allCameras.ToList().ForEach(c => c.RenderSingleFrame(string.Format("cr_{0}.png", c.name)));


            // we also need to modify the shaders used on the tumblers themselves so we can tint them
            // red if cost > funds
            Log.Debug("Modifying cloned TumblerWidget shaders");
            foreach (var t in fundsClone.GetComponentInChildren<Tumblers>().GetComponentsInChildren<Tumbler>())
            {
                var mat = t.renderer.material;

                // this is good enough to do what we want
                mat.shader = Shader.Find("Transparent/Specular");
                mat.SetColor("_Color", Color.black);
                mat.SetColor("_SpecColor", Color.red);
                mat.SetFloat("_Shininess", 0f);
            }

            // now we need to set ourselves up to receive the right events
            Log.Debug("Register for editor ship modified event");
            GameEvents.onEditorShipModified.Add(ShipModified);

            // bugfix: if the editor loads a ship on startup, no event gets sent
            // wait a frame for it to initialize and then calculate cost
            yield return 0;
            ShipModified(null);
        }


        void OnDestroy()
        {
            GameEvents.onEditorShipModified.Remove(ShipModified);
        }


        void ShipModified(ShipConstruct ship)
        {
            Log.Debug("TumblerWidget.ShipModified");

            float dryCost = 0f;
            float fuelCost = 0f;
            float total = EditorLogic.fetch.ship.GetShipCosts(out dryCost, out fuelCost);

            // player might have a strategy active which increases/decreases total ship cost
            var cm = CurrencyModifierQuery.RunQuery(TransactionReasons.VesselRollout, total, 0f, 0f);
            total += cm.GetEffectDelta(Currency.Funds); // it's not a multiplier, it's the difference between current and modified total

            Tumblers tumblers = fundsClone.GetComponentInChildren<Tumblers>();
            tumblers.setValue(total);

            tumblers.GetComponentsInChildren<Tumbler>().ToList().ForEach(t =>
            {
                if (t.renderer != null && t.renderer.material != null)
                    t.renderer.material.SetFloat("_Shininess", total > Funding.Instance.Funds ? 0f : .47f);
            });
        }

#if DEBUG
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                print("taking snapshot");
                var maskcam = Camera.allCameras.First(c => c.name == "UI mask camera");


                maskcam.RenderSingleFrame("uimask_captured.png");
                maskcam.gameObject.PrintComponents();

                var uicam = Camera.allCameras.First(c => c.name == "UI camera");
                foreach (Transform t in uicam.transform)
                    t.parent = null;

                uicam.RenderSingleFrame("uicam_captured.png");
                uicam.gameObject.PrintComponents();
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                print("destroying guilayer");

                var uicam = Camera.allCameras.First(c => c.name == "UI camera");
                Destroy(uicam.GetComponent<GUILayer>());
            }
        }
#endif
    }
}
